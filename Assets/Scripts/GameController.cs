﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    Macros.Direction gameDirection;
    private float moveX = 0f;
    private float moveY = 0f;

    // Update is called once per frame
    void FixedUpdate()
    {
        //comprobamos si se ha accionado alguna direccion
        moveX = Input.GetAxis("Horizontal");
        moveY = Input.GetAxis("Vertical");

        //definimos las prioridades y la direccion
        if (moveX>0f){
            gameDirection = Macros.Direction.Right;
        }else{
            if (moveY>0f){
                gameDirection = Macros.Direction.Up;
            }else{
                gameDirection = Macros.Direction.Down;
            }
            gameDirection = Macros.Direction.Left;
        }
    }
}
